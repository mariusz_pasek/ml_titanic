import pandas as pd
import itertools
from sklearn.neural_network import MLPClassifier
from multiprocessing import Pool


def initialize():
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)


def print_to_html(data, filename):
    pd.set_option('colheader_justify', 'center')

    html_string = '''
    <html>
      <head><title>HTML Pandas Dataframe with CSS</title></head>
      <link rel="stylesheet" type="text/css" href="df_style.css"/>
      <body>
        {table}
      </body>
    </html>.
    '''

    with open(filename, 'w') as f:
        f.write(html_string.format(table=data.to_html(classes='mystyle')))


embark_dict = {
    'C': 1,
    'S': 2,
    'Q': 3,
}

sex_dict = {
    'male': 0,
    'female': 1
}


class Tester:
    def __init__(self, train_dataset, train_labels, validate_dataset, validate_labels, test_dataset, relevant_features):
        self.train_dataset = train_dataset
        self.train_labels = train_labels
        self.validate_dataset = validate_dataset
        self.validate_labels = validate_labels
        self.test_dataset = test_dataset
        self.relevant_features = relevant_features

    def run_for_params(self, activation, hidden_layer_sizes, solver, alpha, learning_rate_init, learning_rate):
        clf = MLPClassifier(activation=activation,
                            hidden_layer_sizes=hidden_layer_sizes,
                            solver=solver,
                            alpha=alpha,
                            learning_rate_init=learning_rate_init,
                            learning_rate=learning_rate,
                            random_state=1,
                            batch_size=200,
                            max_iter=10000,
                            shuffle=True,
                            tol=0.0001,
                            n_iter_no_change=100)

        clf.fit(self.train_dataset[self.relevant_features], self.train_labels)

        train_score = clf.score(self.train_dataset[self.relevant_features], self.train_labels)
        valid_score = clf.score(self.validate_dataset[self.relevant_features], self.validate_labels)

        print('activation:', activation)
        print('hidden_layer_sizes:', hidden_layer_sizes)
        print('solver:', solver)
        print('alpha:', alpha)
        print('learning_rate_init:', learning_rate_init)
        print('learning_rate:', learning_rate)
        print('Train score: {}'.format(train_score))
        print('Valid score: {}'.format(valid_score))
        print()

        results = [int(el) for el in clf.predict(self.test_dataset[self.relevant_features])]
        return results

    def test_params(self, activation, hidden_layer_sizes, solver, alpha, learning_rate_init, learning_rate):
        clf = MLPClassifier(activation=activation,
                            hidden_layer_sizes=hidden_layer_sizes,
                            solver=solver,
                            alpha=alpha,
                            learning_rate_init=learning_rate_init,
                            learning_rate=learning_rate,
                            random_state=1,
                            batch_size=200,
                            max_iter=10000,
                            shuffle=True,
                            tol=0.0001,
                            n_iter_no_change=100)

        clf.fit(self.train_dataset[self.relevant_features], self.train_labels)

        train_score = clf.score(self.train_dataset[self.relevant_features], self.train_labels)
        valid_score = clf.score(self.validate_dataset[self.relevant_features], self.validate_labels)

        print('Train score: {}'.format(train_score))
        print('Valid score: {}'.format(valid_score))


def run_trial(trial):
    relevant_features, train_dataset, train_labels, validate_dataset, validate_labels, (activation, hidden_layer_sizes, solver, alpha, learning_rate_init, learning_rate) = trial

    clf = MLPClassifier(activation=activation,
                        hidden_layer_sizes=hidden_layer_sizes,
                        solver=solver,
                        alpha=alpha,
                        learning_rate_init=learning_rate_init,
                        learning_rate=learning_rate,
                        random_state=1,
                        batch_size=200,
                        max_iter=10000,
                        shuffle=True,
                        tol=0.0001,
                        n_iter_no_change=100)

    clf.fit(train_dataset[relevant_features], train_labels)
    train_score = clf.score(train_dataset[relevant_features], train_labels)
    valid_score = clf.score(validate_dataset[relevant_features], validate_labels)

    results_pd = pd.DataFrame(
        data=[[activation, hidden_layer_sizes, solver, alpha, learning_rate_init, learning_rate,
               train_score, valid_score]],
        columns=['Activation', 'HiddenLayerSize', 'Solver', 'Alpha', 'LearningRateInit', 'LearningRate',
                 'TrainScore',
                 'ValidScore'])
    results_pd.to_csv('model_tuning.csv', mode='a', header=False, index=False)


def check_learning_params(pool_size, relevant_features, train_dataset, train_labels, validate_dataset, validate_labels):
    activations = ['tanh', 'logistic', 'relu']
    hidden_layer_sizess = [el for el in itertools.product(range(6, 13), range(4, 13))]
    solvers = ['lbfgs', 'sgd', 'adam']
    alphas = [0.0005]
    learning_rate_inits = [0.001]
    learning_rates = ['adaptive', 'invscaling']

    tasks = [(relevant_features, train_dataset, train_labels, validate_dataset, validate_labels, el) for el in
             itertools.product(activations, hidden_layer_sizess, solvers, alphas, learning_rate_inits, learning_rates)]

    pd.DataFrame(
        columns=['Activation', 'HiddenLayerSize', 'Solver', 'Alpha', 'LearningRateInit', 'LearningRate', 'TrainScore',
                 'ValidScore']).to_csv('model_tuning.csv', index=False)

    p = Pool(pool_size)
    p.map(run_trial, tasks)


class DataProcessor:
    def __init__(self):
        self.cabin_dict = {}
        self.ticket_dict = {}

    def _load_preprocess(self, dataset):
        average_age = dataset['Age'].mean()

        dataset['Age'] = dataset['Age'].apply(lambda x: x if not pd.isna(x) else average_age)
        dataset['AgeValid'] = dataset['Age'].apply(lambda x: int(not pd.isna(x)))

        dataset['Embarked'] = dataset['Embarked'].apply(lambda x: embark_dict.get(str(x), -1.0))
        dataset['EmbarkedValid'] = dataset['Embarked'].apply(lambda x: int(x != -1.0))

        dataset['Cabin'] = dataset['Cabin'].apply(lambda x: self.cabin_dict.get(str(x), -1.0))
        dataset['CabinValid'] = dataset['Cabin'].apply(lambda x: int(x != -1.0))

        dataset['Sex'] = dataset['Sex'].apply(lambda x: sex_dict.get(str(x), -1.0))
        dataset['SexValid'] = dataset['Sex'].apply(lambda x: int(x != -1.0))

        dataset['Ticket'] = dataset['Ticket'].apply(lambda x: self.ticket_dict.get(str(x), -1.0))
        dataset['TicketValid'] = dataset['Ticket'].apply(lambda x: int(x != -1.0))

        dataset['Fare'] = dataset['Fare'].apply(lambda x: -1.0 if pd.isna(x) else x)

        for column in dataset:
            if column in ['Name', 'Embarked']:
                continue
            max_el = dataset[column].max()
            min_el = dataset[column].min()

            if max_el == min_el:
                continue
            dataset[column] = (dataset[column] - min_el) / (max_el - min_el)

        return dataset[sorted(dataset.columns)]

    def load_train_data(self, filename):
        dataset = pd.read_csv(filename, index_col='PassengerId')

        train_dataset = dataset.sample(frac=0.8, random_state=0)
        validate_dataset = dataset.drop(train_dataset.index)

        self.cabin_dict = {str(x): idx for idx, x in enumerate(train_dataset['Cabin'].unique())}
        self.ticket_dict = {str(x): idx for idx, x in enumerate(train_dataset['Ticket'].unique())}

        train_dataset = self._load_preprocess(train_dataset)
        self.get_special_tickets(train_dataset)
        self.preprocess_data(train_dataset)

        validate_dataset = self._load_preprocess(validate_dataset)
        self.preprocess_data(validate_dataset)

        return dataset, train_dataset, validate_dataset

    def load_test_data(self, filename):
        dataset = pd.read_csv(filename, index_col='PassengerId')
        dataset = self._load_preprocess(dataset)
        return self.preprocess_data(dataset)

    def get_special_tickets(self, dataset):
        survived_in_room = {}
        lived_in_room = {}
        for idx, row in dataset.iterrows():
            if row['Ticket'] not in survived_in_room:
                survived_in_room[row['Ticket']] = 0.
                lived_in_room[row['Ticket']] = 0.
            survived_in_room[row['Ticket']] += row['Survived']
            lived_in_room[row['Ticket']] += 1.

    def preprocess_data(self, dataset):
        for key in embark_dict.keys():
            dataset['Embarked_{}'.format(key)] = dataset['Embarked'].apply(lambda e: int(e == embark_dict[key]))

        return dataset[sorted(dataset.columns)]
