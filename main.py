import pathlib
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from sklearn.neural_network import MLPClassifier

import utils

utils.initialize()

data_processor = utils.DataProcessor()

dataset, train_dataset, validate_dataset = data_processor.load_train_data('train.csv')
test_dataset = data_processor.load_test_data('test.csv')

relevant_features = ['Age', 'Cabin', 'Fare', 'Parch', 'Pclass', 'Sex', 'SibSp']
# relevant_features = [el for el in train_dataset.columns if el not in ['Survived', 'Name']]

train_labels = train_dataset.pop('Survived')
validate_labels = validate_dataset.pop('Survived')

tester = utils.Tester(train_dataset, train_labels, validate_dataset, validate_labels, test_dataset, relevant_features)

params_list = [
    ['relu', (9, 4), 'sgd', 0.0005, 0.001, 'adaptive'],
    # ['relu', (7, 7), 'sgd', 0.0005, 0.001, 'adaptive'],
    # ['tanh', (10, 6), 'sgd', 0.0005, 0.001, 'adaptive'],
    # ['tanh', (10, 7), 'lbfgs', 0.0005, 0.001, 'invscaling'],
    # ['tanh', (6, 11), 'lbfgs', 0.0005, 0.001, 'invscaling'],
    # ['relu', (12, 10), 'sgd', 0.0005, 0.001, 'adaptive'],
    # ['tanh', (12, 4), 'lbfgs', 0.0005, 0.001, 'adaptive']
]

results_list = [tester.run_for_params(*params) for params in params_list]

merged_results = [int(sum(line) >= 0.5) for line in zip(*results_list)]
results_pd = pd.DataFrame(data=zip(test_dataset.index, merged_results), columns=['PassengerId', 'Survived'])
results_pd.to_csv('result.csv', index=False)
